# AsToRia: Refractive Index

This C library loads the refractive index by wavelength stored in atrri
format. See `atrri.5` for format specification.

## Requirements

- C compiler
- POSIX make
- pkg-config
- [RSys](https://gitlab.com/vaplv/rsys)
- [mandoc](https://mandoc.bsd.lv)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.1

- Write the man page directly in mdoc's roff macros, instead of using
  the scdoc markup language as a source for man roff documents.
- Replace CMake by Makefile as build system.
- Update compiler and linker flags to increase the security and
  robustness of generated binaries.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.0.2

- Fix compilation warnings detected by gcc 1.
- Use scdoc rather than asciidoc as file format for man sources.

### Version 0.0.1

Set the minimum version of CMake to 3.1. Previously, it was set to the
now obsolete version 2.8.

## Copyright

Copyright (C) 2022, 2023 |Méso|Star (contact@meso-star.com)  
Copyright (C) 2021 Centre National de la Recherche Scientifique (CNRS)

## License

AtrRI is free software released under the GPL v3+ license: GNU GPL
version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
