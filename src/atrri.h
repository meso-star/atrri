/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ATRRI_H
#define ATRRI_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(ATRRI_SHARED_BUILD) /* Build shared library */
  #define ATRRI_API extern EXPORT_SYM
#elif defined(ATRRI_STATIC) /* Use/build static library */
  #define ATRRI_API extern LOCAL_SYM
#else /* Use shared library */
  #define ATRRI_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the atrri function `Func'
 * returns an error. One should use this macro on sth function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define ATRRI(Func) ASSERT(atrri_ ## Func == RES_OK)
#else
  #define ATRRI(Func) atrri_ ## Func
#endif

struct atrri_refractive_index {
  double wavelength; /* In nanometer */
  double n; /* Real part */
  double kappa; /* Imaginary part */
};
#define ATRRI_REFRACTIVE_INDEX_NULL__ {0,0,0}
static const struct atrri_refractive_index ATRRI_REFRACTIVE_INDEX_NULL =
  ATRRI_REFRACTIVE_INDEX_NULL__;

struct atrri_desc {
  /* List of indices sorted in ascending order wrt the wavelength */
  const struct atrri_refractive_index* indices;
  size_t nindices;
};
#define ATRRI_DESC_NULL__ {NULL, 0}
static const struct atrri_desc ATRRI_DESC_NULL = ATRRI_DESC_NULL__;

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

/* Forware declaration of opaque data type */
struct atrri;

BEGIN_DECLS

/*******************************************************************************
 * AtrRI API
 ******************************************************************************/
ATRRI_API res_T
atrri_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* allocator, /* NULL <=> use default allocator */
   const int verbose, /* Verbosity level */
   struct atrri** atrri);

ATRRI_API res_T
atrri_ref_get
  (struct atrri* atrri);

ATRRI_API res_T
atrri_ref_put
  (struct atrri* attrtp);

ATRRI_API res_T
atrri_load
  (struct atrri* atrri,
   const char* path);

ATRRI_API res_T
atrri_load_stream
  (struct atrri* atrri,
   FILE* stream,
   const char* stream_name); /* Can be NULL */

ATRRI_API res_T
atrri_fetch_refractive_index
  (const struct atrri* atrri,
   const double wavelength,
   struct atrri_refractive_index* refract_id);

ATRRI_API res_T
atrri_get_desc
  (const struct atrri* atrri,
   struct atrri_desc* desc);

END_DECLS

#endif /* ATRRI_H */
