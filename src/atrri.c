/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2021 Centre National de la Recherche Scientifique
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* strtok_r support */

#include "atrri.h"
#include "atrri_c.h"
#include "atrri_log.h"

#include <rsys/algorithm.h>
#include <rsys/cstr.h>
#include <rsys/text_reader.h>

#include <errno.h>
#include <string.h>

/*******************************************************************************
 * Local functions
 ******************************************************************************/
static INLINE int
cmp_refractive_index(const void* key, const void* entry)
{
  const double wavelength = *((const double*)key);
  const struct atrri_refractive_index* id = entry;

  if(wavelength < id->wavelength) {
    return -1;
  } else if(wavelength > id->wavelength) {
    return 1;
  } else {
    return 0;
  }
}

static INLINE void
reset_atrri(struct atrri* atrri)
{
  ASSERT(atrri);
  darray_refractive_id_clear(&atrri->refract_ids);
}

static res_T
parse_line(struct atrri* atrri, struct txtrdr* txtrdr)
{
  struct atrri_refractive_index refract_id = ATRRI_REFRACTIVE_INDEX_NULL;
  char* tk = NULL;
  char* tk_ctx = NULL;
  size_t nrefract_ids = 0;
  res_T res = RES_OK;
  ASSERT(atrri && txtrdr);

  /* Parse the wavelength */
  tk = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  ASSERT(tk); /* A line should exist since it was parsed by txtrdr */
  res = cstr_to_double(tk, &refract_id.wavelength);
  if(res == RES_OK && refract_id.wavelength < 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(atrri, "%s:%lu: invalid wavelength `%s' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      tk, res_to_cstr(res));
    goto error;
  }

  nrefract_ids = darray_refractive_id_size_get(&atrri->refract_ids);
  if(nrefract_ids != 0) {
    /* Check that the indices are sorted in ascending order wrt wavelength */
    const struct atrri_refractive_index* prev_refract_id =
      darray_refractive_id_cdata_get(&atrri->refract_ids) + nrefract_ids - 1;
    if(prev_refract_id->wavelength >= refract_id.wavelength) {
      log_err(atrri,
        "%s:%lu: refractive indices must be sorted in ascending order "
        "acording to their corresponding wavelength.\n",
        txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
      res = RES_BAD_ARG;
      goto error;
    }
  }

  /* Parse the real part */
  tk = strtok_r(NULL, " \t", &tk_ctx);
  if(!tk) {
    log_err(atrri,
      "%s:%lu: could not read the real part of the refractive index.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }
  res = cstr_to_double(tk, &refract_id.n);
  if(res == RES_OK && refract_id.n < 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(atrri,
      "%s:%lu: invalid real part of the refractive index `%s' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      tk, res_to_cstr(res));
    goto error;
  }

  /* Parse the imaginary part */
  tk = strtok_r(NULL, " \t", &tk_ctx);
  if(!tk) {
    log_err(atrri,
      "%s:%lu: could not read the imaginary part of the refractive index.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }
  res = cstr_to_double(tk, &refract_id.kappa);
  if(res == RES_OK && refract_id.kappa < 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(atrri,
      "%s:%lu: invalid imaginary part of the refractive index `%s' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      tk, res_to_cstr(res));
    goto error;
  }

  tk = strtok_r(NULL, " \t", &tk_ctx);
  if(tk) {
    log_warn(atrri, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

  /* Register the parsed refractive index */
  res = darray_refractive_id_push_back(&atrri->refract_ids, &refract_id);
  if(res != RES_OK) {
    log_err(atrri, "%s:%lu: could not register the refractive index -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
load_stream(struct atrri* atrri, FILE* stream, const char* stream_name)
{
  struct txtrdr* txtrdr = NULL;
  res_T res = RES_OK;
  ASSERT(atrri && stream && stream_name);

  reset_atrri(atrri);

  res = txtrdr_stream(atrri->allocator, stream, stream_name, '#', &txtrdr);
  if(res != RES_OK) {
    log_err(atrri, "%s: could not create the text reader -- %s.\n",
      stream_name, res_to_cstr(res));
  }

  for(;;) {
    res = txtrdr_read_line(txtrdr);
    if(res != RES_OK) {
      log_err(atrri, "%s: could not read the line `%lu' -- %s.\n",
        txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
        res_to_cstr(res));
      goto error;
    }

    if(!txtrdr_get_cline(txtrdr)) break; /* No more parsed line */

    res = parse_line(atrri, txtrdr);
    if(res != RES_OK) goto error;
  }

  if(darray_refractive_id_size_get(&atrri->refract_ids) == 0) {
    log_err(atrri, "%s: empty file.\n", stream_name);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  if(txtrdr) txtrdr_ref_put(txtrdr);
  return res;
error:
  goto exit;
}

static void
release_atrri(ref_T* ref)
{
  struct atrri* atrri;
  ASSERT(ref);
  atrri = CONTAINER_OF(ref, struct atrri, ref);
  darray_refractive_id_release(&atrri->refract_ids);
  if(atrri->logger == &atrri->logger__) logger_release(&atrri->logger__);
  MEM_RM(atrri->allocator, atrri);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
atrri_create
  (struct logger* logger, /* NULL <=> use default logger */
   struct mem_allocator* mem_allocator, /* NULL <=> use default allocator */
   const int verbose, /* Verbosity level */
   struct atrri** out_atrri)
{
  struct atrri* atrri = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!out_atrri) {
    res = RES_BAD_ARG;
    goto error;
  }

  allocator = mem_allocator ? mem_allocator : &mem_default_allocator;
  atrri = MEM_CALLOC(allocator, 1, sizeof(*atrri));
  if(!atrri) {
    if(verbose) {
      #define ERR_STR "Could not allocate the AtrRI device.\n"
      if(logger) {
        logger_print(logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&atrri->ref);
  atrri->allocator = allocator;
  atrri->verbose = verbose;
  if(logger) {
    atrri->logger = logger;
  } else {
    setup_log_default(atrri);
  }

  darray_refractive_id_init(atrri->allocator, &atrri->refract_ids);

exit:
  if(out_atrri) *out_atrri = atrri;
  return res;
error:
  if(atrri) {
    ATRRI(ref_put(atrri));
    atrri = NULL;
  }
  goto exit;
}

res_T
atrri_ref_get(struct atrri* atrri)
{
  if(!atrri) return RES_BAD_ARG;
  ref_get(&atrri->ref);
  return RES_OK;
}

res_T
atrri_ref_put(struct atrri* atrri)
{
  if(!atrri) return RES_BAD_ARG;
  ref_put(&atrri->ref, release_atrri);
  return RES_OK;
}

res_T
atrri_load(struct atrri* atrri, const char* path)
{
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!atrri || !path) {
    res = RES_BAD_ARG;
    goto error;
  }

  file = fopen(path, "r");
  if(!file) {
    log_err(atrri, "%s: error opening file -- %s.\n",
      path, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(atrri, file, path);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
atrri_load_stream(struct atrri* atrri, FILE* stream, const char* stream_name)
{
  res_T res = RES_OK;

  if(!atrri || !stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = load_stream(atrri, stream, stream_name ? stream_name : "stream");
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

res_T
atrri_get_desc(const struct atrri* atrri, struct atrri_desc* desc)
{
  if(!atrri || !desc) return RES_BAD_ARG;
  desc->indices = darray_refractive_id_cdata_get(&atrri->refract_ids);
  desc->nindices = darray_refractive_id_size_get(&atrri->refract_ids);
  return RES_OK;
}

res_T
atrri_fetch_refractive_index
  (const struct atrri* atrri,
   const double wavelength,
   struct atrri_refractive_index* refract_id)
{
  struct atrri_desc desc = ATRRI_DESC_NULL;
  struct atrri_refractive_index* found = NULL;
  res_T res = RES_OK;

  if(!atrri || wavelength < 0 || !refract_id) {
    res = RES_BAD_ARG;
    goto error;
  }

  ATRRI(get_desc(atrri, &desc));

  /* Search for the registered refractive index whose corresponding wavelength
   * is greater than or equal to the submitted wavelength */
  found = search_lower_bound(&wavelength, desc.indices, desc.nindices,
    sizeof(*desc.indices), cmp_refractive_index);

   if(!found) {
    /* All refractive indices are defined for wavelengths less than the
     * submitted one. Clamp to the last registered refractive index  */
    ASSERT(desc.nindices);
    *refract_id = desc.indices[desc.nindices-1];

  } else if(found == desc.indices) {
    /* The submitted wavelength is less than the first wavelength for which a
     * refractive index is registered. Clamp to the first registered refractive
     * index */
    *refract_id = desc.indices[0];

  } else  {
    const size_t inext = (size_t)(found - desc.indices);
    const size_t iprev = inext - 1;
    const struct atrri_refractive_index* next = desc.indices + inext;
    const struct atrri_refractive_index* prev = desc.indices + iprev;
    ASSERT(next->wavelength >= wavelength);
    ASSERT(prev->wavelength < wavelength);

    if(next->wavelength == wavelength) {
      /* The submitted wavelength strictly match a registered wavelength */
      *refract_id = *next;

    } else {
      /* Linearly interpolate the refractive index */
      const double len = next->wavelength - prev->wavelength;
      const double u = CLAMP((wavelength - prev->wavelength) / len, 0, 1);
      ASSERT(len > 0);
      refract_id->wavelength = wavelength;
      refract_id->n = u*(prev->n - next->n) + next->n;
      refract_id->kappa = u*(prev->kappa - next->kappa) + next->kappa;
    }
  }

exit:
  return res;
error:
  goto exit;
}
